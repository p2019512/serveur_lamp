﻿# Bienvenue sur le module Serveur Lamp

Ce module sert uniquement à l'installation du serveur LAMP sur votre machine virtuelle à l'aide d'un dépôt distant. Ce document est  utile pour vous guider lors de l'installation de ce module et pour éclaircir certains points.




# Déploiement du module 
## Prérequis
Le module serveur_lamp nécessite d'être installé sur une machine vierge et principalement Ubuntu.

Pour le déploiement du module il est nécessaire d'installer Docker Engine. On va commencer par installer le référentiel docker à l'aide des commandes suivantes :

## Référentiel Docker
```
sudo apt-get update
```

Puis : 
```
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```
   
Ensuite on ajout la clé GPG officielle de Docker : 

```
     curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```
Après utilisez la commande suivant pour configurer le référentiel stable :

```
 echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
## Installer Docker Engine

Pour finir l'installation de Docker Engine il suffit d'exécuter ces commandes :
```
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
Enfin Docker Engine sera installé sur votre machine Ubuntu.


## Git clone
Pour avoir le module sur votre VM Ubuntu et notamment le fichier docker-compose.yml utile pour l'installation du serveur LAMP, effectué la commande :
git@forge.univ-lyon1.fr:p2019512/serveur_lamp.git

Ensuite vous aurez une arborescence de ce type : 
```
`-- serveur_lamp
	|-- README.md
	|-- docker-compose.yml
```

Lancer l'execution du module via la commande suivante en root :
```
 docker-compose up
```
Et enfin vous aurez un serveur LAMP installé avec notamment une base de donnée (mariadb), un serveur Apache et un serveur PHP.


# Information sur le module

## Les répertoires 

Le répertoire "serveur_lamp" contient le fichier readme que l'on lit actuellement puis le fichier de configuration pour l'installation du serveur LAMP par docker.








